import requests

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0',
    'Accept': '*/*',
    'Accept-Language': 'en-US,en;q=0.5',
    'Accept-Encoding': 'gzip, deflate',
    'Referer': 'https://booking.uz.gov.ua/?from=2200001&to=2204580&date=2018-04-06&time=00%3A00&url=train-list',
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'X-Requested-With': 'XMLHttpRequest'
    }

data = {
    'from':'2200001',
    'to':'2204580',
    'date':'2018-04-06',
    'time':'00:00'
    }


responce = requests.post('https://booking.uz.gov.ua/train_search/', headers=headers, data=data )

j = responce.json()
if j["data"].get("warning"):
    print(j["data"]["warning"])
else:
    for train in j["data"]["list"]:
        x = [ [places["id"],places["places"]] for places in train["types"]]
        if x:
            print(train["num"], "[", train["from"]["date"], train["from"]["time"],train["from"]["station"], "]" , "->", "[",train["to"]["time"],train["to"]["station"], "]", train["travelTime"], x)
